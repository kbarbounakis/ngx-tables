
export declare interface TableColumnConfiguration {
    key?: string;
    name?: string;
    title: string;
    placeholder?: string;
    sortable?: boolean;
    class?: string;
    defaultContent?: string;
    formatter?: string;
    formatString?: string;
    hidden?: boolean;
    property?: string;
    order?: string;
    formatOptions?: any;
    formatters?: Array<any>;
    virtual?: boolean;
    export?: boolean;
    optional?: boolean;
}

export declare interface TableConfiguration {
    criteria: any;
    defaults: any;
    title?: string;
    model: string;
    searchExpression?: string;
    columns: Array<TableColumnConfiguration>;
    searches: Array<any>;
    selectable?: boolean;
    multipleSelect?: boolean;
}

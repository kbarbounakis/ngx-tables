import { Injectable } from '@angular/core';
import { TemplatePipe } from '@universis/common';

@Injectable({
  providedIn: 'root'
})
export class AdvancedFilterValueProvider {

  public values = { };
  private _template: TemplatePipe;

  constructor() {
    this._template = new TemplatePipe();
  }

  buildFilter(filter) {
    if (filter) {
      return this._template.transform(filter, this.values);
    }
  }

  async asyncBuildFilter(filter) {
    if (filter) {
      return this._template.transform(filter, await this.getValues());
    }
  }

  async getValues() {
    return this.values;
  }

}

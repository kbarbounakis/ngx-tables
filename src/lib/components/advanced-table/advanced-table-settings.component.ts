import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { AdvancedTableComponent } from './advanced-table.component';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedTableService } from '../../tables.activated-table.service';
import { UserStorageService } from '@universis/common';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-advanced-table-settings',
  templateUrl: './advanced-table-settings.component.html',
  encapsulation: ViewEncapsulation.None
})

export class AdvancedTableSettingsComponent implements OnInit, AfterViewInit {

  @Input() table?: AdvancedTableComponent;
  @ViewChild('modal_settings_template') modal_settings_template?: ElementRef;
  modalRef?: BsModalRef;
  storageKey: any;
  params: any = this._activatedRoute.params;
  private savedTableSettings: any;
  form?: FormGroup;
  columns: any[] = [];
  tableColumns: any;
  activeTableConfig: any;

  constructor(protected _context: AngularDataContext,
              protected _activatedRoute: ActivatedRoute,
              private modalService: BsModalService,
              private userStorage: UserStorageService,
              private formBuilder: FormBuilder,
              private _activatedTable: ActivatedTableService) {
  }
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.activeTableConfig = this._activatedTable.activeTable && this._activatedTable.activeTable.getConfig();
    this.storageKey = 'registrar/settings/' + this.activeTableConfig.title + '/table/columns';
    this.form = this.formBuilder.group({columns: new FormArray([], this.minSelectedColumns(1))});
    this.userStorage.getItem(this.storageKey).then((data) => {
      if (data) {
        this.savedTableSettings = data.value;
        if (this.savedTableSettings) {
          if (this._activatedTable.activeTable) {
            this._activatedTable.activeTable.handleColumns(this.savedTableSettings);
          }
        }
        this.addCheckboxes();
      }
    });
  }
  private addCheckboxes() {
    this.tableColumns = this.activeTableConfig.columns.slice(1);
    this.tableColumns.forEach ((column, i) => {
      if (!column.hidden) {
        const control = {
          id: column.name,
          name: column.title
        };
        this.columns.push(control);
      }
    });
    if (this.savedTableSettings) {
      this.columns.forEach ((column, i) => {
        column.check = false;
        column.check = this.savedTableSettings.includes(column.id);
      });
      this.columns.map((column, i) => {
        const control = new FormControl(column.check);
        if (this.form) {
          (this.form.controls.columns as FormArray).push(control);
        }
      });
    }
  }

  openTableSettings(table_settings_modal: TemplateRef<any>) {
    this.modalRef = this.modalService.show(table_settings_modal);
  }

  saveTableSettings() {
    if (this.form == null) {
      return;
    }
    const selectedColumnsIds = this.form.value.columns
      .map((v, i) => v ? this.columns[i].id : null)
      .filter(v => v !== null);
    this.userStorage.setItem(this.storageKey, selectedColumnsIds);
    if (this._activatedTable.activeTable) {
      this._activatedTable.activeTable.handleColumns(selectedColumnsIds);
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  dismiss() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  minSelectedColumns(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);

      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }
}

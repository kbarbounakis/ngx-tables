import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../advanced-table/advanced-table.component';
import { Subscription } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'table-simple-header-tools',
  template: `
  <ng-content></ng-content>
  `,
  styles: []
})
export class SimpleHeaderToolsComponent {
  //
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-table-simple-header',
  templateUrl: './table-simple-header.component.html',
  styles: []
})
export class TableSimpleHeaderComponent implements OnInit, OnDestroy, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.table) {
      if (changes.table.currentValue == null) {
        if (this.dataLoadSubscription) {
          this.dataLoadSubscription.unsubscribe();
          return;
        }
      }
      // tslint:disable-next-line:triple-equals
      if (changes.table.currentValue != changes.table.previousValue) {
        this.dataLoadSubscription = changes.table.currentValue.load.subscribe((data: AdvancedTableDataResult) => {
          this.recordsTotal = data.recordsTotal;
        });
      }
    }
  }
  ngOnDestroy(): void {
    if (this.dataLoadSubscription) {
      this.dataLoadSubscription.unsubscribe();
    }
  }

  @Input() table?: AdvancedTableComponent;
  public recordsTotal?: number;
  public dataLoadSubscription?: Subscription;
  constructor() { }

  ngOnInit() {
    //
  }

}
